import React from 'react';
import Avatar from '../avatar/avatar';
import MessageContent from '../messageContent/messageContent';
import EditButton from '../editButton/editButton';

import './message.css';

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditButtonShow: false
    };
  
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isEditButtonShow: !this.state.isEditButtonShow
    });
  }

  getEditButton(id) {
    return this.state.isEditButtonShow ?
      <EditButton
        onClick={(e) => this.props.onEdit(id)}
      /> :
      null;
  }

  render() {
    let className = this.props.isOwn ? "message current-member" : "message";
    className = this.props.isLiked ? className + " Liked" : className;
    const id = this.props.id;

    if (this.props.isOwn) {
      return (
        <li
          className={className}
          onMouseEnter={this.toggle}
          onMouseLeave={this.toggle}
        >
          <MessageContent {...this.props} />
          <button
          className="message-action"
            onClick={(e) => this.props.onDelete(id)}
          >
            Delete
          </button>
          {this.getEditButton(id)}
        </li>
      )
    }

    return (
      <li
        className={className}
        onMouseEnter={this.toggle}
        onMouseLeave={this.toggle}
      >
        <Avatar
          avatar={this.props.avatar}
          user={this.props.user}
        />
        <MessageContent {...this.props} />
        {this.getEditButton(id)}
        <button className="Like-button" onClick={(e) => this.props.onLike(id)}>Like</button>
      </li>
    )
  }
}

export default Message;