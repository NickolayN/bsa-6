import React from 'react';
import './avatar.css';

function Avatar(props) {
  return (
    <img
      className="Avatar"
      src={props.avatar}
      alt={props.user}
    />
  );
}

export default Avatar;