import React from 'react';
import './header.css';

class Header extends React.Component {
  formatDate(date) {
    return new Date(date).toLocaleTimeString();
  }
  render() {
    const {participants, messages, lastMessageDate} = this.props;

    return (
      <div className="Header">
        <div>participants: {participants}</div>
        <div>messages: {messages}</div>
        <div>lastMessage: {this.formatDate(lastMessageDate)}</div>
      </div>
    )
  }
}

export default Header;