const getMessageId = () => Date.now() * 10;
const getDate = () => Date.now();

export default {
  getMessageId,
  getDate
};