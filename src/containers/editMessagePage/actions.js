import { SHOW_PAGE, HIDE_PAGE } from "./actionTypes";

export const showPage = (id) => ({
  type: SHOW_PAGE,
  payload: {
    id
  }
});

export const hidePage = () => ({
  type: HIDE_PAGE
});