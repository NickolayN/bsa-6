import React from 'react';
import './App.css';
import Chat from './containers/chat/index';

function App() {
  return (
    <div className="App">
      <Chat />
    </div>
  );
}

export default App;
